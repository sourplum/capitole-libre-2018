#! /usr/bin/env sh

echo "Copy static landrush host.json configuration...\n"

echo '{
  "capitole.dev": "172.16.0.41",
  "41.0.16.172.in-addr.arpa": "capitole.dev"
}' > ~/.vagrant.d/data/landrush/hosts.json

echo "...done"
