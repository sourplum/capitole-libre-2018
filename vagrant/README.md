## Vagrant

He who write an Ansible Playbook:
    + Always test it on `staging` machines
    + `staging` machines are generated with Vagrant


Inside this directory create your machines

```bash
vagrant up
```

If Landrush failed to setup properly (if ping `metapolis.dev` failed)

```bash
./fix-landrush.sh
```

Refer to [SSH Config file]('../inventory/sshconfig') to connect to your machines
