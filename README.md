# Capitole du Libre 2018 - Ansible playbooks

Ansible is an automation tool used to administrate servers using `cookbooks` also called `playbooks`.
`Playbooks` are idempotent, running them multiple times will not have a different outcome.

## Structure

+ [ansible](./ansible)
    + [ansible-common]( ./ansible/ansible-common )         *Start here: exchange keys with server, set hostname, firewall with ufw, ...*
    + [ansible-docker]( ./ansible/ansible-docker )         *Install docker and docker-compose*
    + [ansible-nginx-ssl]( ./ansible/ansible-nginx-ssl )   *Install nginx, enforce security, install certbot and auto-renew certificates*
    + [ansible-gitlab]( ./ansible/ansible-gitlab )         *Deploy Gitlab omnibus docker, with local backups under ./backups*
    + [ansible-nextcloud]( ./ansible/ansible-nextcloud )   *Deploy Nextcloud + Collabora Code + Postgres docker, with local backups under ./backups*
    + [ansible-monitoring]( ./ansible/ansible-monitoring ) *Deploy Prometheus + Grafana + Exporters docker, with default dashboard for docker and Gitlab*
    + [ansible-peertube]( ./ansible/ansible-peertube )     [WIP]
+ [docker]( ./docker )       *Personnal docker images here (sent to hub.docker.com)*
+ [inventory]( ./inventory ) *Example sshconfig as used in playbooks host*
+ [vagrant]( ./vagrant )     *Vagrantfile to deploy staging environment*

