## Docker image of Homepage

Small homepage with links to Monitoring tools.

## Environments

`SITE_SERVER_NAME`: Domain name to be used

example: foobar.example.com (omit the protocol)

## Why?

Grafana, Prometheus and Alertmanager under sub locations.

Needed a landing page with links to them.
