FROM alpine:3.10
LABEL maintainer, Cedric Bonhomme bonhomme@neptune.one

ENV ACME_AGREE="false"
ENV ENABLE_TELEMETRY="false"
ENV ALPINE_MIRROR="http://mirrors.ircam.fr/pub/alpine"

# Another mirror
RUN echo "$ALPINE_MIRROR/v3.10/main" > /etc/apk/repositories; \
	echo "$ALPINE_MIRROR/v3.10/community" >> /etc/apk/repositories

# Install dependencies
RUN apk add --no-cache --update caddy

WORKDIR /var/www/html

# Copy app src code to /usr/src/app
ADD ./app /usr/src/app
# Copy caddy reverse proxy conf
COPY Caddyfile /etc/Caddyfile
# Copy docker entrypoint
COPY docker-entrypoint.sh /usr/bin/docker-entrypoint.sh

EXPOSE 8080

ENTRYPOINT ["/usr/bin/docker-entrypoint.sh"]
CMD ["caddy","-conf","/etc/Caddyfile","-log","stdout"]
