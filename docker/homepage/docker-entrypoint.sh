#!/bin/sh

set -e

if [ "$1" = "caddy" ]; then
  if [ -n "$SITE_SERVER_NAME+x" ]; then
      echo "Copying src code to /var/www/html..."
      cp -r /usr/src/app/* /var/www/html/

      echo "Generating index.html file..."
      sed -e "s#{{SITE_SERVER_NAME}}#$SITE_SERVER_NAME#g" \
	  -e "s#{{TITLE}}#${TITLE-"Health Homepage"}#g" \
	  "/usr/src/app/index.html" > "/var/www/html/index.html"

      echo "...done! Website is ready to use."
  else
      echo &2>1 "Error, SITE_SERVER_NAME variable missing. Terminating!"
      exit 1
  fi
fi

exec "$@"
