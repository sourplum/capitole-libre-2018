Certbot
=========

Generate Let's Encrypt certificates with certbot

Requirements
------------

The role requires Nginx to be installed with certbot-nginx plugin on the target hosts.

Role Variables
--------------

The role require those variables to be set in order to obtain a certificate

+ `is_production`
    Default to false, See [Let's Encrypt, staging environment](https://letsencrypt.org/docs/staging-environment/) to know why.
    Set to true to generate a real SSL certificate.
+ `certbot_admin_email`
    Default to admin@example.com
    Email to receive expiration notification of certificates from Let's Encrypt.
+ `certbot_domain`
    Default to foobar.example.com
    Domain name to be registered.

Dependencies
------------

+ `nginxinc.nginx` to install Nginx
+ `nginx` to configure nginx

Example Playbook
----------------

Generate production ready certificates for git.enterprise.com:

    - hosts: servers
      vars:
         is_production: true
         certbot_admin_email: johndoe@enterprise.com
         certbot_domain: git.enterprise.com
      roles:
         - { role: common }
         - { role: nginx }
         - { role: certbot }

License
-------

BSD
