## Peertube Ansible playbook

Ansible playbook to deploy Peertube instance

Federated (ActivityPub) video streaming platform using P2P (BitTorrent) directly in the web browser with WebTorrent and Angular.

## Install

```bash
ansible-playbook site.yml -i staging
```
