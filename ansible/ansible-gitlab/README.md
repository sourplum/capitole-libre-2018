## Purpose

1. Install Gitlab CE
2. Serve it with Nginx

## Install

```bash
$ ansible-playbook -i staging install.yml
```

## Expected result

Gitlab repository manager up and running with project management and monitoring.
Admin has account.
