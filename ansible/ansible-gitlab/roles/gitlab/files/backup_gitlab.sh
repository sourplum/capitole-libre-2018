#! /usr/bin/env bash

#------------------------------------------------#
# Gitlab backup                                  #
#------------------------------------------------#

gitlab_container=$1    # gitlab
gitlab_image=$2        # gilab/gitlab-ce:10.5.2-ce.0
gitlab_backup_path= $3 # /opt/gitlab/backups

# Check if the container is up and running
function is_container_running {
    docker inspect -f '{{.State.Running}}' $gitlab_container;
}
# Check if the container has correct image and version
function is_gitlab_image {
    docker inspect -f "{{eq .Config.Image \"$gitlab_image\"}}" $gitlab_container;
}

# Ensure folders are present
function create_backup_folders {
    mkdir -p /backups/gitlab/{app,secret};
}

# Backup application
function application_backup {
    docker exec -t $gitlab_container \
        gitlab-rake gitlab:backup:create;
}

# Backup configuration and secrets
function configuration_secrets_backup {
    docker exec -t $gitlab_container /bin/sh -c \
        "umask 0077; tar cfz $gitlab_backup_path/secret/$(date "+etc-gitlab-\%s.tgz") -C / etc/gitlab";
}

if [ "$(is_container_running)" == "true" ] && [ "$(is_gitlab_image)" == "true" ]; then
    create_backup_folders;
    echo "Backup folder created";
    application_backup;
    echo "Success, created application backup";
    configuration_secrets_backup;
    echo "Success, created configuration and secrets backup";
    exit 0;
else
    echo "Error: Could not backup Gitlab. Verify that the container $gitlab_container is up and running. Gitlab container needs to be running $gitlab_image image";
    exit 1; # Error status code (Bash)
fi
