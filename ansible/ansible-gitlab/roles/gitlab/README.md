Gitlab
======

Gitlab up and running

Requirements
------------

The role requires Nginx and Docker to be installed on the target hosts.

Dependencies
------------

+ `nginxinc.nginx-oss` to install Nginx
+ `nginx` to configure nginx
+ `docker` to install docker and docker-compose

Example Playbook
----------------

Serves Gitlab with Nginx and auto-signed certificates

    - hosts: servers
      roles:
        - { role: nginxinc.nginx-oss, tags: "install" }
        - { role: nginx                               }
        - { role: docker                              }
        - { role: gitlab                              }

License
-------

BSD
