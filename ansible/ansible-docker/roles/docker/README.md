Docker
=========

Install Docker and docker-compose from official Docker repository.

Requirements
------------

Debian 9

Example Playbook
----------------

Generate production ready certificates for git.enterprise.com:

    - hosts: servers
      roles:
        - { role: docker }

License
-------

BSD
