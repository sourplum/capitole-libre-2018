Docker TLS
==========

Configure Docker to protect the TCP socket, instead of the UNIX socket.

Requirements
------------

Docker must be installed on the host machine.

Role Variables
--------------

+ `docker_host`: Host running the docker daemon
    default to 172.17.0.1
+ `docker_ca`:
    `passphrase`: passphrase used for docker self signed certificates
    default to "foobar"

Example Playbook
----------------

    - hosts: servers
      roles:
        - { role: docker     }
        - { role: docker-tls }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
