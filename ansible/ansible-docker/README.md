## Playbook purpose

+ Install Docker and Docker-compose
+ Secure Docker with TLS certificates (useful for Docker-in-Docker)

## Install

```bash
$ ansible-playbook site.yml -i staging
```

## Expected result

On the host machine, `docker` daemon is up and running.
`docker info` returns info on the installation.
`docker-compose` is also available to use.

**If docker-tls role used**:

User that deployed the play book has certificates available under `~/.docker` directory.
By default `docker` connects via the TCP socket with a check of certificates by the daemon.
