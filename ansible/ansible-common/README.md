## Purpose

1. Setup deploy user with sudo rights
2. Secure server with iptables rules.

## Preparation

Copy public keys under

`./public_keys/host/user.pub`

And add user to `common_ssh_users` under `./group_vars/host/vars`

	Nota: foobar user is set as an example, its public_keys is not valid

#### Example

Alice adds her public keys for the host foobar

```
# ./public_keys/foobar/alice.pub

ssh-rsa AAABBBCCCDDD1234 alice@home
```

```
# /group_vars/foobar/vars file

common_ssh_users:
  - alice
```

## Install

```bash
$ ansible-playbook -i site.yml -i staging
```

## Expected result

Host only authorize connection from:
22 for ssh,
80 and 443 for web services.
