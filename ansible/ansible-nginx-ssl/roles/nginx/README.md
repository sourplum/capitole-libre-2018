Nginx
=========

Configure Nginx reserve proxy
Php support integrated
Configured for improved security

Dependencies
------------

Official role for NGINX

+ `nginxinc.nginx-oss`


Example Playbook
----------------

Install Nginx with A+ SSL note:

    - hosts: servers
      roles:
         - { role: common }
         - { role: nginx }

License
-------

BSD
