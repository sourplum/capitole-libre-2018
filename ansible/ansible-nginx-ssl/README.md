# Nginx and Certbot

Install Nginx, Certbot.
Configure Nginx with secure parameters.
Generate certificates with Certbot.

## Install

```bash
$ ansible-playbook site.yml -i staging
```

If you have an error like:
- `ERROR! the role 'nginxinc.nginx-oss' was not found in ...`

Install `nginxinc.nginx-oss` with `ansible-galaxy`:
```bash
$ ansible-galaxy install nginxinc.nginx
```
