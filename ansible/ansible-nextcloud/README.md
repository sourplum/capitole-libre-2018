## Nextcloud

Ansible playbook to deploy Nextcloud instance

Nextcloud is a cloud storage web application to keep files synchronised between
the server and peers.

## Install

```bash
ansible-playbook site.yml -i staging
```
