#! /usr/bin/env bash

#------------------------------------------------#
#                    Postgres                    #
#------------------------------------------------#

postgres_container=$1
postgres_image=$2
postgres_db=$3
postgres_backups_path=$4

function is_container_running {
    docker inspect -f '{{.State.Running}}' $postgres_container;
}

function is_postgres_image {
    docker inspect -f "{{eq .Config.Image \"$postgres_image\"}}" $postgres_container;
}

function dump_database {
    docker exec -t $postgres_container \
        pg_dump $postgres_db > "$postgres_backups_path/nextcloud_dump.sql";
}

if [ "$(is_container_running)" == "true" ] && [ "$(is_postgres_image)" == "true" ]; then
    dump_database;
    echo "Success, database $postgres_db dump created at $postgres_backups_path/nextcloud_dump.sql";
    exit 0;
else
    echo "Error: Could not dump database. Verify that the container $postgres_container is up and running with $postgres_image image";
    exit 1; # Error status code (Bash)
fi
