## Inventory

List all machines here

Use the provided [SSH Config file]('./sshconfig') to connect to them
Append this file to `~/.ssh/config`

## Requirement

In order to connect, you will need to generate a keyring under `~/.ssh` folder
Use the utility script [new ssh]('./new-ssh.sh')

```bash
./new-ssh.sh host-01
```

