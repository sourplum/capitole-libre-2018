#!/usr/bin/env sh

SERVER_NAME=$1
FILE_OUTPUT=~/.ssh/$SERVER_NAME

exec ssh-keygen -C "$(whoami)@$(hostname)-$(date -I)" -f $FILE_OUTPUT
